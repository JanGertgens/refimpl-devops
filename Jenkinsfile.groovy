node {
    stage("checkout") {
        checkout([$class: 'GitSCM', branches: [[name: '*/master']], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'abbb1acb-58a1-411f-8e0c-44cb3e2faea1', url: 'git@bitbucket.org:JanGertgens/jang-refimpl.git']]])
    }
    stage("Build and Test") {
        sh 'mvn clean package'
        junit 'target/surefire-reports/*.xml'
    }
    stage("Docker build") {
        sh 'docker build -t $REGISTRY/refimpl:$VERSION --build-arg appVersion=$VERSION .'

    }
    stage("Docker push") {
        sh 'docker push $REGISTRY/refimpl:$VERSION'
    }
    stage("Deploy to docker Swarm") {
        input 'Deploy'
        sh 'ssh docker@192.168.99.101 "docker service update refimpl --image $REGISTRY/refimpl:$VERSION"'
    }
}