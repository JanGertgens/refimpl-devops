node {
    stage("Checkout") {
        git branch: '**', credentialsId: '1177765c-3f5d-448a-9d10-8cbfad18ca68', url: 'git@bitbucket.org:JanGertgens/auswertung.git'

    }
    def branch= sh(returnStdout: true, script: 'git rev-parse --abbrev-ref HEAD').trim()
    stage("Build and Test") {
        sh "echo $branch"
        sh 'mvn clean package'
        junit 'target/surefire-reports/*.xml'
    }
    stage("cloud foundry login") {
        sh 'cf login -a https://api.local.pcfdev.io --skip-ssl-validation -u $CFUSER -p $CFPASS '
        sh "cf create-space $branch"
        sh "cf target -s $branch"

    }
    stage("create-cups") {
        try {
            sh 'cf cups mongoDB -p \'{"uri":"10.0.2.15:27017"}\''
        } catch(e) {
            currentBuild.result='success'
            sh "echo skipping step"
        }
    }

    stage("cf push") {
        sh "cf push"
    }
}