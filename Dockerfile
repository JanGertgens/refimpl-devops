FROM anapsix/alpine-java:8

ARG appVersion=0.0.1-SNAPSHOT

ENV VERSION=${appVersion}

ADD target/refimpl-${VERSION}.jar /app.jar

ENV STAGE docker

ENTRYPOINT ["java", "-jar", "app.jar"]

EXPOSE 8001
