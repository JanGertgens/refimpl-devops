package devops.refimpl.Controller;

import devops.refimpl.Utils.ShellCommandExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by jge on 11.01.17.
 */
@RestController
public class RestApi {

    @Autowired
    private Environment environment;

    @Autowired
    private ShellCommandExecutor shellCommandExecutor;

    @Autowired
    private HttpServletRequest request;


    @Value("${deployment.stage}")
    private String stage;


    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public
    @ResponseBody
    String getStatus() {
        String hostnameCommand = "cat /etc/hostname";
        String output = shellCommandExecutor.executeCommand(hostnameCommand);
        return "Hostname of the container:  " + output +
                "\nEnvironment string: " + stage;
    }


}
