package devops.refimpl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RefimplApplication {

	public static void main(String[] args) {
		SpringApplication.run(RefimplApplication.class, args);
	}
}
